import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from '../product-list/product-list.component';
//import { ProductComponent } from '../product/product.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { FavouritesComponent } from '../favourites/favourites.component';
import { ProductViewComponent } from '../product-view/product-view.component';
import { SortPipe } from '../sort.pipe';
import { NumericDirective } from '../numeric.directive';
import { ProductHostDirective } from '../product-host.directive';

@NgModule({
	declarations: [
		ProductListComponent
	],
	imports: [
		CommonModule,
		ProductDetailComponent,
		FavouritesComponent,
		ProductViewComponent,
		SortPipe,
		NumericDirective,
		ProductHostDirective
	],
	exports: [ProductListComponent]
})
export class ProductsModule { }
