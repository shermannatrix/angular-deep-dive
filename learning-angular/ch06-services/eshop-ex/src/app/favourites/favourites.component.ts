import { Component, Host, OnInit, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Product } from '../product';
import { ProductsService } from '../products.service';
import { favouritesFactory } from '../favourites';
import { ProductViewService } from '../product-view/product-view.service';

@Component({
	selector: 'app-favourites',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './favourites.component.html',
	styleUrl: './favourites.component.css',
	providers: [
		{
			provide: ProductsService, 
			useFactory: favouritesFactory(true),
			deps: [ProductViewService] 
		}
	]
})
export class FavouritesComponent implements OnInit {
	products: Product[] = [];

	constructor(private productService: ProductsService) { }

	ngOnInit(): void {
		this.products = this.productService.getProducts();
	}
}
