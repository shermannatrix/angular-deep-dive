import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { Product } from '../product';
import { ProductHostDirective } from '../product-host.directive';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css',
  viewProviders: [ProductsService]
})
export class ProductListComponent implements OnInit, AfterViewInit {
  @ViewChild(ProductDetailComponent) productDetail: ProductDetailComponent | undefined;

  products: Product[] = [];

  selectedProduct: Product | undefined;

  constructor(private productService: ProductsService) {
    this.productService = new ProductsService();
  }

  ngOnInit(): void {
    this.products = this.productService.getProducts();
  }

  ngAfterViewInit(): void {
    if (this.productDetail) {
      console.log(this.productDetail.product);
    }
  }

  // trackByProducts(index: number, name: string): string {
  //   return this.productDetail.name;
  // }

  onBuy(name: string) {
    window.alert(`You just bought ${this.selectedProduct?.name}!`);
  }
}
