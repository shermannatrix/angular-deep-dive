import { Component, Inject } from '@angular/core';
import { APP_CONFIG, appSettings, AppConfig } from './app.config';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrl: './app.component.css',
	providers: [
		{ provide: APP_CONFIG, useValue: appSettings }
	]
})
export class AppComponent {
	title = 'Learning Angular';

	constructor(@Inject(APP_CONFIG) config: AppConfig) {}
}
