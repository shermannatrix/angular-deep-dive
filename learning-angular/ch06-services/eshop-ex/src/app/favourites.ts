import { FavouritesService } from './favourites/favourites.service';
import { ProductsService } from './products.service';
import { ProductViewService } from './product-view/product-view.service';

export function favouritesFactory(isFavourite: boolean) {
	return (productViewService: ProductViewService) => {
		if (isFavourite) {
			return new FavouritesService();
		}

		return new ProductsService();
	};
}