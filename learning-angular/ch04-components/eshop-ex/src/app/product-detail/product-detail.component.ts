import { 
	Component, 
	Input, 
	OnInit,
	OnDestroy,
	Output, 
	EventEmitter, 
	OnChanges,
	SimpleChanges,
	ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
	selector: 'app-product-detail',
	standalone: true,
	imports: [CommonModule],
	templateUrl: './product-detail.component.html',
	styleUrl: './product-detail.component.css',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailComponent 
	implements OnInit, OnDestroy, OnChanges {
	@Input() name = '';
	@Output() bought = new EventEmitter<string>();

	constructor() {
		console.log(`Name is ${this.name} is the constructor`);
	}

	ngOnInit(): void {
			console.log(`Name is ${this.name} in the ngOnInit`);
	}

	ngOnChanges(changes: SimpleChanges): void {
			const product = changes['name'];
			if (!product.isFirstChange()) {
				const oldValue = product.previousValue;
				const newValue = product.currentValue;
				console.log(`Product changed from ${oldValue} to ${newValue}`);
			}
	}

	ngOnDestroy(): void {
			
	}

	get productName(): string {
		console.log(`Get ${this.name}`);
		return this.name;
	}

	buy() {
		this.bought.emit(this.name);
	}
}
