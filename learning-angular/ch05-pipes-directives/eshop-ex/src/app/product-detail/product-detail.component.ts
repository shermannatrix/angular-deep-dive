import { 
	Component, 
	Input, 
	OnInit,
	OnDestroy,
	Output, 
	EventEmitter, 
	OnChanges,
	SimpleChanges,
	ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Product } from '../product';
import { NumericDirective } from '../numeric.directive';

@Component({
	selector: 'app-product-detail',
	standalone: true,
	imports: [CommonModule, NumericDirective],
	templateUrl: './product-detail.component.html',
	styleUrl: './product-detail.component.css',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailComponent 
	implements OnInit, OnDestroy, OnChanges {
	@Input() product: Product | undefined;
	@Output() bought = new EventEmitter<string>();
	today = new Date();

	constructor() {
		console.log(`Name is ${this.product?.name} is the constructor`);
	}

	ngOnInit(): void {
			console.log(`Name is ${this.product?.name} in the ngOnInit`);
	}

	ngOnChanges(changes: SimpleChanges): void {
			const product = changes['product'];
			if (!product.isFirstChange()) {
				const oldValue = product.previousValue.name;
				const newValue = product.currentValue.name;
				console.log(`Product changed from ${oldValue} to ${newValue}`);
			}
	}

	ngOnDestroy(): void {
			
	}

	get productName(): string {
		console.log(`Get ${this.product?.name}`);
		return this.product!.name;
	}

	buy() {
		this.bought.emit(this.product?.name);
	}
}
