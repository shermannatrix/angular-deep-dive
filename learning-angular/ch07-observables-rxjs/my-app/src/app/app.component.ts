import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { Observable } from 'rxjs';
import { KeyLoggerComponent } from './key-logger/key-logger.component';

@Component({
	selector: 'app-root',
	standalone: true,
	imports: [CommonModule, RouterOutlet, KeyLoggerComponent],
	templateUrl: './app.component.html',
	styleUrl: './app.component.css'
})
export class AppComponent {
	title = 'Learning Angular';
	// title$ = new Observable(observer => {
	// 	setInterval(() => {
	// 		observer.next();
	// 	}, 2000);
	// });
	description = 'Hello World';

	constructor() {
		//this.title$.subscribe(this.setTitle);
	}

	private setTitle = () => {
		//const timestamp = new Date().getMilliseconds();
		this.title = `Learning Angular`;
	}

	private changeTitle(callback: Function) {
		setTimeout(() => {
			callback();
		}, 2000);
	}

	private onComplete() {
		return new Promise<void>(resolve => {
			setInterval(() => {
				resolve();
			}, 2000);
		});
	}
}
