var Greeter = /** @class */ (function () {
    function Greeter() {
    }
    Greeter.sayHello = function (name) {
        console.log("Hello ".concat(name));
    };
    return Greeter;
}());
Greeter.sayHello('Sherman');
//# sourceMappingURL=greeter.js.map