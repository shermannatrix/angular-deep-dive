var Product = /** @class */ (function () {
    function Product() {
    }
    return Product;
}());
var getProducts = function (id) {
    if (typeof id === 'number') {
        return { id: 123 };
    }
    else {
        return [{ id: 123 }, { id: 567 }];
    }
};
var result1 = getProducts(123);
console.log(result1);
var result2 = getProducts();
console.log(result2);
