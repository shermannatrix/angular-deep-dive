class Product {
	id: number;
}

const getProducts = function<T>(id?:T):
	T extends number ? Product : Product[] {
	if (typeof id === 'number') {
		return { id: 123 } as any;
	} else {
		return [{ id: 123 }, { id: 567 }] as any;
	}
}

const result1 = getProducts(123);
console.log(result1);

const result2 = getProducts();
console.log(result2);