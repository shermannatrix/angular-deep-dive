interface Person {
	name: string;
	age: number;
}

const persons: Person[] = [
	{ name: 'John', age: 32 },
	{ name: 'Mary', age: 33 }
];

function filterBy<T, P extends keyof T> (
	property: P,
	value: T[P],
	array: T[]) {
	return array.filter(item => item[property] === value);
}

console.log(filterBy('name', 'John', persons));

// console.log(filterBy('lastName', 'John', persons))	// in correct invocation