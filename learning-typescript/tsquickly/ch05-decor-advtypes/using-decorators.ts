function UIcomponent (html: string) {
	console.log(`The decorator received ${html}\n`);

	return function (target: Function) {
		console.log(`Someone wants to create a UI component from \n${target}`);
	}
}

@UIcomponent('<h1>Hello Shopper!</h1>')
class Shopper {
	constructor(private name: string) {}
}