type constructorMixin = { new (...args: any[]): {} };

function useSalutation(salutation: string) {
	return function <T extends constructorMixin> (target: T) {
		return class extends target {
			name: string;
			private message = 'Hello ' + salutation;

			sayHello() { console.log(`${this.message} ${this.name}`); }
		}
	}
}

@useSalutation("Mr.")
class Greeter {
	constructor(public name: string) { }
	sayHello() { console.log(`Hello ${this.name}`); }
}

const grt = new Greeter('Smith');
grt.sayHello();