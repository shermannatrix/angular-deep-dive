type Modifiable<T> = {
	-readonly[P in keyof T]: T[P];
};

interface ROPerson {
	readonly name: string;
	readonly age: number;
}

const worker1: ROPerson = { name: "John", age: 25 };

// worker1.age = 27; <- Results in error

const worker2: Modifiable<ROPerson> = { name: "John", age: 25 };

worker2.age = 27;	// No errors