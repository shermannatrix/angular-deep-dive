"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Car = /** @class */ (function () {
    function Car() {
    }
    Car.prototype.startEngine = function () {
        return true;
    };
    Car.prototype.stopEngine = function () {
        return true;
    };
    Car.prototype.brake = function () {
        return true;
    };
    Car.prototype.accelerate = function (speed) {
        console.log("Driving faster!");
    };
    Car.prototype.honk = function (howLong) {
        console.log("Beep beep yeah!");
    };
    return Car;
}());
var SecretServiceCar = /** @class */ (function () {
    function SecretServiceCar() {
    }
    SecretServiceCar.prototype.startEngine = function () {
        return true;
    };
    SecretServiceCar.prototype.stopEngine = function () {
        return true;
    };
    SecretServiceCar.prototype.brake = function () {
        return true;
    };
    SecretServiceCar.prototype.accelerate = function (speed) {
        console.log("Driving faster");
    };
    SecretServiceCar.prototype.honk = function (howLong) {
        console.log("Beep beep yeah!");
    };
    SecretServiceCar.prototype.fly = function (howHigh) {
        console.log("Flying ".concat(howHigh, " feet high"));
    };
    SecretServiceCar.prototype.land = function () {
        console.log("Landing. Fasten your belts.");
    };
    SecretServiceCar.prototype.swim = function (howFar) {
        console.log("Swimming ".concat(howFar, " feet."));
    };
    return SecretServiceCar;
}());
var car = new Car();
car.startEngine();
car.accelerate(20);
car.honk(2);
var ssCar = new SecretServiceCar();
ssCar.startEngine();
ssCar.fly(100);
ssCar.land();
