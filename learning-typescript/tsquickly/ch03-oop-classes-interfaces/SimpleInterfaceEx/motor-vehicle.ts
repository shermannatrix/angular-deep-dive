interface MotorVehicle {
	startEngine(): boolean;
	stopEngine(): boolean;
	brake(): boolean;
	accelerate(speed: number): void;
	honk(howLong: number): void;
}

interface Flyable {
	fly (howHigh: number);
	land();
}

interface Swimmable {
	swim(howFar: number);
}

export { MotorVehicle, Flyable, Swimmable };