var AppState = /** @class */ (function () {
    // A private constructor prevents using the new operator with AppState
    function AppState() {
        this.counter = 0;
    }
    // This is the only method to get an instance of AppState
    AppState.getInstance = function () {
        if (AppState.instanceRef === undefined) {
            // Instantiates the AppState object if it doesn't exist yet.
            AppState.instanceRef = new AppState();
        }
        return AppState.instanceRef;
    };
    return AppState;
}());
var appState1 = AppState.getInstance();
var appState2 = AppState.getInstance();
appState1.counter++;
appState1.counter++;
appState2.counter++;
appState2.counter++;
console.log(appState1.counter);
console.log(appState2.counter);
