var BasicProductService = /** @class */ (function () {
    function BasicProductService() {
    }
    BasicProductService.prototype.getProducts = function (id) {
        if (typeof id === 'number') {
            console.log("Getting the product info for ".concat(id));
        }
        else {
            console.log("Getting all products");
        }
    };
    return BasicProductService;
}());
var prodService = new BasicProductService();
prodService.getProducts(123);
prodService.getProducts();
