var ProductService = /** @class */ (function () {
    function ProductService() {
    }
    ProductService.prototype.getProducts = function (product) {
        if (typeof product === "number") {
            console.log("Getting the product info for id ".concat(product));
            return {
                id: product,
                description: 'great product'
            };
        }
        else if (typeof product === "string") {
            console.log("Getting product with description ".concat(product));
            return [
                { id: 123, description: 'blue jeans' },
                { id: 789, description: 'blue jeans' }
            ];
        }
        else {
            return {
                id: -1, description: 'Error: getProducts() accept only number or string as args'
            };
        }
    };
    return ProductService;
}());
var newProdService = new ProductService();
console.log(newProdService.getProducts(123));
console.log(newProdService.getProducts('blue jeans'));
