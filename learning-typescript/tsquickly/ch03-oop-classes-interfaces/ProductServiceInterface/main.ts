interface Product {
	id: number;
	description: string;
}

interface IProductService {
	getProducts(): Product[];
	getProductById(id: number): Product;
}

class NewProductService implements IProductService {
	getProducts(): Product[] {
		return [];
	}

	getProductById(id: number): Product {
		return { id: 123, description: 'Good product' };
	}
}