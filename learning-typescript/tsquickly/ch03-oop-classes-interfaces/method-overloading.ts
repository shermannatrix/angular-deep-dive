class BasicProductService {
	getProducts(): void;
	getProducts(id: number): void;
	getProducts(id?: number) {
		if (typeof id === 'number') {
			console.log(`Getting the product info for ${id}`);
		} else {
			console.log(`Getting all products`);
		}
	}
}

const prodService = new BasicProductService();

prodService.getProducts(123);
prodService.getProducts();