var printMe = function (content) {
    console.log(content);
    return content;
};
var a = printMe("Hello");
var Person = /** @class */ (function () {
    function Person(name) {
        this.name = name;
    }
    return Person;
}());
var b = printMe(new Person("Joe"));
