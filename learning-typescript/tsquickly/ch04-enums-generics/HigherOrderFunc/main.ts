const outerFunc = (someValue: number) => (multiplier: number) => someValue * multiplier;

const innerFunc = outerFunc(10);	// invoking (someValue: number)

let result = innerFunc(5);	// invoking (multiplier: number)

console.log(result);