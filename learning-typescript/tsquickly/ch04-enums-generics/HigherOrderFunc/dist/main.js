var outerFunc = function (someValue) { return function (multiplier) { return someValue * multiplier; }; };
var innerFunc = outerFunc(10); // invoking (someValue: number)
var result = innerFunc(5); // invoking (multiplier: number)
console.log(result);
