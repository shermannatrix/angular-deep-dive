var Pair = /** @class */ (function () {
    function Pair(key, value) {
        this.key = key;
        this.value = value;
    }
    return Pair;
}());
function compare(pair1, pair2) {
    return pair1.key === pair2.key &&
        pair1.value === pair2.value;
}
var p1 = new Pair(1, "Apple");
var p2 = new Pair(1, "Orange");
// Comparing apples to orange
console.log(compare(p1, p2));
var p3 = new Pair("first", "Apple");
var p4 = new Pair("first", "Apple");
// Comparing apples to apples
console.log(compare(p3, p4));
