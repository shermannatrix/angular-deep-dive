interface Person {
	firstName: string;
	lastName: string;
	age: number;
}

function savePerson(person: Person): void {
	console.log('Saving ', person);
}

const p: Person = {
	firstName: "Sherman",
	lastName: "Chen",
	age: 25
};

savePerson(p);