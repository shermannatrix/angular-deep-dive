type Height = number;
type Weight = number;

type Patient = {
	name: string;
	height: Height;
	weight?: Weight;
}

let patient: Patient = {
	name: 'Sherman Chen',
	height: 1.71
}

console.log(patient);