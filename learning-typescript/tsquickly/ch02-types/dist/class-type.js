var Block = /** @class */ (function () {
    function Block(index, previousHash, timestamp, data) {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = timestamp;
        this.data = data;
        var _a = this.mine(), nonce = _a.nonce, hash = _a.hash;
        this.nonce = nonce;
        this.hash = hash;
    }
    Block.prototype.mine = function () {
        // TO DO
        return { nonce: 1, hash: "" };
    };
    return Block;
}());
